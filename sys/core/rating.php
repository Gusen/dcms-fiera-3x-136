<?php
  
	class rating{
	
	#	Добавление и обработка
	static function add($id = 0,$rating = null, $flud = 5,$pr = null)
	{
		//если пусто задаем время 
		if ($_SESSION['rating'] == null) 
			$_SESSION['rating'] = time();
	
		if ($id > 0 and $_SESSION['rating'] <= time()	)
		{
			//массив с юзером
			$user_arr = get_user($id);
			//округляем
			$rating = number_format($rating, 2);
			//делим на процент 
			if ($pr != null)
			$rating = $rating - $rating * "0.$pr";
			//запрос в базу
		   	query("update `user` set `rating` = '". ($user_arr['rating'] + $rating) ."' where `id` = ". $user_arr['id']);
			 //Удаляем кэш
			 cache_delete::user($user_arr['id']);
			//от флуда
			$_SESSION['rating'] = time() + $flud;
			  
		}
	} 
	
	
		#	Убавление и обработка
	static function minus($id = 0,$rating = null, $flud = 5,$pr = null)
	{
		//если пусто задаем время 
		if ($_SESSION['rating_minus'] == null) 
			$_SESSION['rating_minus'] = time();
	
		if ($id > 0 and $_SESSION['rating_minus'] <= time()	)
		{
			//массив с юзером
			$user_arr = get_user($id);
			//округляем
			$rating = number_format($rating, 2);
			//делим на процент 
			if ($pr != null)
			$rating = $rating - $rating * "0.$pr";
			//запрос в базу
		   	query("update `user` set `rating` = '". ($user_arr['rating'] - $rating) ."' where `id` = ". $user_arr['id']);
			 //Удаляем кэш
			 cache_delete::user($user_arr['id']);
			//от флуда
			$_SESSION['rating_minus'] = time() + $flud;
			  
		}
	}

	 
	#	Вывод 
	static function output($id = 0)
	{
		//обработка и вывод правильного рейтинга
		//на случай если все таки где то были косяки
		$user_arr = get_user($id);
		
		return number_format($user_arr['rating'],2);
	}
	
	
	//получаем рейтинг  за сообщение 
	static function msgnum($nums = 0.00)
	{
		$nums = mb_strlen($nums, 'UTF-8');
		$nums = ($nums / 100000);
		return number_format($nums,2); 
	}
	 
	
	
	#	Добавление и обработка
	static function ban($id = 0)
	{
		//если пусто задаем время 
		if (@$_SESSION['rating_ban'] == null) 
			$_SESSION['rating_ban'] = time();
	
		if ($id > 0 and @$_SESSION['rating_ban'] <= time())
		{
			//массив с юзером
			$user_arr = get_user($id);
			if ($user_arr['rating'] > 0.00)
			{
			    $rating = $user_arr['rating'];
				//округляем
				$rating = number_format($rating, 2);
				//вычисляем нужный процент
				$rating = ($user_arr['rating'] - $user_arr['rating'] * 0.50);
				//запрос в базу
				query("update `user` set `rating` = '". $rating ."' where `id` = ". $user_arr['id'] );
				//удаляем кэш  
				cache_delete::user($user_arr['id']);
			 }
			//от флуда
			$_SESSION['rating_ban'] = time() + 60;
		}
	} 
	
	
	}

	